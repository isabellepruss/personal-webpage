import { createBrowserRouter } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Layout from "./pages/Layout";
import ErrorPage from "./pages/ErrorPage";
import Research from "./components/Research";
import Resume from "./components/Resume";
import Education from "./components/Education";
import Contact from "./components/Contact";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    errorElement: <ErrorPage />,
    children: [
      { path: "/", element: <HomePage /> },
      { path: "ResearchandSkills/", element: <Research /> },
      { path: "/Resume/", element: <Resume /> },
      { path: "/Education/", element: <Education /> },
      { path: "/Contact/", element: <Contact /> },
    ],
  },
]);

export default router;
