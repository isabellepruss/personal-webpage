import {
  Box,
  Flex,
  Grid,
  GridItem,
  HStack,
  Heading,
  VStack,
} from "@chakra-ui/react";
import Bio from "../components/Bio";
import Contact from "../components/Contact";
import ProfPic from "../components/ProfPic";

const App = () => {
  return (
    <GridItem area="main">
      <Box>
        <Heading as="h1" size="3xl" paddingBottom={5}>
          Isabelle Pruss
        </Heading>
        <HStack>
          <ProfPic />
          <Box padding={10}>
            <Bio />
          </Box>
        </HStack>
      </Box>
    </GridItem>
  );
};

export default App;
