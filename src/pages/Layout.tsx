import {
  Box,
  ColorModeContext,
  Grid,
  GridItem,
  HStack,
  Show,
} from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import PageLinks from "../components/PageLinks";
import ColorModeSwitch from "../components/ColorModeSwitch";

const Layout = () => {
  return (
    <Grid
      h="500px"
      templateRows="repeat(6, 1fr)"
      templateColumns="repeat(6, 1fr)"
      gap={4}
    >
      <GridItem rowSpan={5} colSpan={1} padding={10}>
        <ColorModeSwitch />
        <PageLinks />
      </GridItem>

      <GridItem rowSpan={5} colSpan={5} padding={10}>
        <Box>
          <Outlet />
        </Box>
      </GridItem>
    </Grid>
  );
};

export default Layout;
