import { Text } from "@chakra-ui/react";

const Research = () => {
  return (
    <>
      <Text as="b" fontSize="2xl">
        Research and Skills
      </Text>
      <br />
      <Text as="b" fontSize="1xl">
        Missouri Institute for Defense and Energy
      </Text>
      <Text fontSize="sm">
        At UMKC, I have a research position in the cyber operations group of The
        Missouri Institute for Defense and Energy. My efforts include aiding in
        in the development of an automated process for use in electronic warfare
        for a classified military inteligence project. More specifically, I work
        to develop the front-end interface that users will interact with to
        deploy cyber attacks and monitor those in progress. I have gained
        experience with the React Library, JavaScript, RESTful APIs, command
        line usage, virtual machines, and the basics of white-hat hacking.
      </Text>
      <br />
      <Text fontSize="sm">
        Through this research experience and other school projects related to
        software developement, I have been fortunate to gain the many skills.
        <br />
        - Python
        <br />
        - C++
        <br />
        - Java
        <br />
        - React
        <br />
        - command line
        <br />
        - scripting languanges
        <br />
        - Unix / Linux operating systems
        <br />- Creation and manipulation of database systems
      </Text>
      <br />
    </>
  );
};

export default Research;
