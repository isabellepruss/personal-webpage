import { HStack } from "@chakra-ui/react";
import ColorModeSwitch from "./ColorModeSwitch";

const Navbar = () => {
  return (
    <HStack padding="10px">
      <ColorModeSwitch />
    </HStack>
  );
};

export default Navbar;
