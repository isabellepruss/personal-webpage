import { HStack, Heading, Icon, Link, List, ListItem } from "@chakra-ui/react";
import {
  BsBook,
  BsHouseAdd,
  BsJournalBookmark,
  BsLaptop,
  BsPerson,
} from "react-icons/bs";

const PageLinks = () => {
  return (
    <>
      <List paddingLeft="5px">
        <ListItem>
          <HStack padding={2}>
            <Icon as={BsHouseAdd} boxSize="30px" />
            <Link href="/" fontSize="x-large">
              Home
            </Link>
          </HStack>
        </ListItem>
        <ListItem>
          <HStack padding={2}>
            <Icon as={BsBook} boxSize="30px" />
            <Link href="/Education/" fontSize="x-large">
              Education
            </Link>
          </HStack>
        </ListItem>
        <ListItem>
          <HStack padding={2}>
            <Icon as={BsLaptop} boxSize="30px" />
            <Link href="/ResearchandSkills/" fontSize="x-large">
              Research
            </Link>
          </HStack>
        </ListItem>
        <ListItem>
          <HStack padding={2}>
            <Icon as={BsJournalBookmark} boxSize="30px" />
            <Link href="/Resume/" fontSize="x-large">
              Resume
            </Link>
          </HStack>
        </ListItem>
        <ListItem>
          <HStack padding={2}>
            <Icon as={BsPerson} boxSize="30px" />
            <Link href="/Contact/" fontSize="x-large">
              Contact
            </Link>
          </HStack>
        </ListItem>
      </List>
    </>
  );
};

export default PageLinks;
