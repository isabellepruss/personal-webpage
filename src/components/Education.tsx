import { Text } from "@chakra-ui/react";

const Education = () => {
  return (
    <>
      <br />
      <Text as="b" fontSize="2xl">
        Education Background
      </Text>
      <Text>B.S. Physics & B.A. Computer Science</Text>
      <Text> Relevant Coursework</Text>
      <br />

      <Text as="b" fontSize="1xl">
        Math 250 - Calculus III:
      </Text>
      <Text fontSize="sm">
        Solid analytic geometry, multiple-variable and vector-valued functions,
        partial derivatives, Langrage multipliers, multiple integrals,
        Jacobians, line and surface integrals, Green's Theorem, the Divergence
        Theorem, and Stoke's Theorem.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Math 300 - Linear Algebra:
      </Text>
      <Text fontSize="sm">
        This course introduces a branch of algebra developed from the theory of
        finding simultaneous solutions of a collection of linear equations,
        emphasizing fundamental concepts, calculations, and applications. Topics
        include matrix algebra, real vector spaces, linear transformations,
        determinants, the study of eigenvalues and eigenvectors, and the
        exploration of orthogonality and other applications.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 250 - Phsyics for Scientists and Engineers
      </Text>
      <Text fontSize="sm">
        Introduction to electricity and magnetism, light and optics and modern
        physics. Four hours lecture and two hours laboratory per week.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 310 - Mechanics I:
      </Text>
      <Text fontSize="sm">
        Advanced statics and dynamic of particles and rigid bodies including
        gravitation.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 311 - Mechanics II:
      </Text>
      <Text fontSize="sm">
        Continuation of Mechanics I, including mechanics of continuous media,
        LaGrange's equations, tensor algebra, and theory of small vibrations.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 330 - Mechanics II:
      </Text>
      <Text fontSize="sm">
        Introduction to mathematical and numerical methods used in the
        theoretical modeling of physical systems. Treatments of linear systems
        in scientific and engineering applications will be emphasized.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 350 - Modern Physics with Engineering:
      </Text>
      <Text fontSize="sm">
        An introduction to the theories that revolutionized science and
        technology in the twentieth century. Topics include special and general
        relativity, introductory quantum mechanics and atomic structure.
        Inventions and applications based on these are also examined.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 385L - Physics of Electronics:
      </Text>
      <Text fontSize="sm">
        An introduction to the solid-state physics of basic electronic
        components and their operation through both theory and practical lab
        work.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 410 - Thermal Physics:
      </Text>
      <Text fontSize="sm">
        A study of the laws of thermodynamics and their applications, with an
        introduction to kinetic theory. Statistical methods are emphasized.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 460 - Electricity and Magnetism I:
      </Text>
      <Text fontSize="sm">
        Static electric fields in free space and material media; Kirchoff’s laws
        and direct current circuits; static and magnetic fields.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 461 - Electricity and Magnetism II:
      </Text>
      <Text fontSize="sm">
        Magnetostatics; alternating current circuits; Maxwell’s equation and
        radiation; special relativity; topics in electromagnetism.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        Physics 472 - Introduction to Quantum Mechanics:
      </Text>
      <Text fontSize="sm">
        Introduction to the theory and applications of quantum mechanics with
        emphasis on the mathematical treatment of modern physics.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 101 - Problem Solving and Programming I:
      </Text>
      <Text fontSize="sm">
        Problem solving, algorithms, and program design. Use of structured
        programming, lists, control structures, recursion, objects and files in
        Python. Introduction to graphical interface programming. Coding,
        testing, and debugging using a modern development environment.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 101 - Problem Solving and Programming I:
      </Text>
      <Text fontSize="sm">
        Problem solving, algorithms, and program design. Use of structured
        programming, lists, control structures, recursion, objects and files in
        Python. Introduction to graphical interface programming. Coding,
        testing, and debugging using a modern development environment.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 191 - Discrete Structures I:
      </Text>
      <Text fontSize="sm">
        Mathematic logic, sets, relations, functions, mathematical induction,
        algebraic structures with emphasis on computing applications.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 201 - Problem Solving and Programming II:
      </Text>
      <Text fontSize="sm">
        Problem solving and programming using classes and objects. Algorithm
        efficiency, abstract data types, searching and sorting, templates,
        pointers, linked lists, stacks and queues implemented in C++.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 281 - Computer Architecture and Organization:
      </Text>
      <Text fontSize="sm">
        Digital Logic and data representation, process architecture and
        instruction sequencing, memory hierarchy and bus-interfaces and
        functional organization.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 291 - Discrete Structures II:
      </Text>
      <Text fontSize="sm">
        Recurrence relations and their use in the analysis of algorithms.
        Graphs, trees, and network flow models. Introduction to finite state
        machines, grammars, and automata.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 303 - Data Structures:
      </Text>
      <Text fontSize="sm">
        Linear and hierarchical data structures, including stacks, queues,
        lists, trees, priority queues, advanced tree structures, hashing tables,
        dictionaries and disjoint-set. Abstractions and strategies for efficient
        implementation will be discussed. Linear and hierarchical algorithms
        will be studied as well as recursion and various searching and sorting
        algorithms. Programming concepts include object orientation, concurrency
        and parallel programming. Several in-depth projects in C++ will be
        required.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 304 - Ethics and Professionalism:
      </Text>
      <Text fontSize="sm">
        This course examines the societal and ethical obligations of computer
        science, information technology, and electrical/computer engineering
        practice. Topics include obligations of professional practice, digital
        privacy, intellectual property, and ethical issues around computer
        security, reliability, and networking.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 431 - Introduction to Operating Systems:
      </Text>
      <Text fontSize="sm">
        This course covers concurrency and control of asynchronous processes,
        deadlocks, memory management, processor and disk scheduling, x86
        assembly language, parallel processing, security, protection, and file
        system organization in operating systems.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 449 - Foundations of Software Engineering:
      </Text>
      <Text fontSize="sm">
        An introduction to the fundamental principles and practices of software
        engineering. Students will learn a systematic and disciplined process
        for developing software and apply it to a small project. The focus will
        be on the agile development process, software requirements, software
        design, and unit testing.
      </Text>
      <br />

      <Text as="b" fontSize="1xl">
        CS 449 - Database Management Systems:
      </Text>
      <Text fontSize="sm">
        This course covers database architecture, data independence, schema,
        Entity-Relationship and relational database modeling, relational and
        calculus, SQL, file organization, relational database design, physical
        database organization, query processing and optimization, transaction
        structure and execution, concurrency control mechanisms, database
        recovery, and database security.
      </Text>
      <br />
    </>
  );
};

export default Education;
