import { Text } from "@chakra-ui/react";

const Bio = () => {
  return (
    <>
      <br />
      <Text>
        Hi, my name is Isabelle Pruss and I am a senior at UMKC pursuing degrees
        in Physics and Computer Science. Alongside my studies I am an
        undergraduate research assistant for The Missouri Institute for Defense
        and Energy at UMKC on their cyber operations team. My role is to assist
        in the development of automated methodologies for offensive cyber
        attacks. I hope to continue to work in the realm of cyber security,
        defense contracting, and the like after graduation.
      </Text>
    </>
  );
};

export default Bio;
