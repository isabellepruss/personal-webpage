import { Image } from "@chakra-ui/react";
import resume from "../assets/resume.png";
const Resume = () => {
  return (
    <>
      <br />
      <Image src={resume} boxSize="900px" />
    </>
  );
};

export default Resume;
