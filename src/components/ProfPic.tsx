import { Image } from "@chakra-ui/react";
import picturelight2 from "../assets/picture-light2.jpg";

const ProfPic = () => {
  return <Image src={picturelight2} boxSize="200px" objectFit="cover" />;
};

export default ProfPic;
