import { Text, HStack, Box } from "@chakra-ui/react";

const Contact = () => {
  return (
    <Box padding={20}>
      <HStack>
        <Text as="b" fontSize="3xl">
          Email:
        </Text>
        <Text fontSize="3xl">isabelle.pruss@outlook.com</Text>
      </HStack>

      <HStack>
        <Text as="b" fontSize="3xl">
          Phone:
        </Text>
        <Text fontSize="3xl">913.475.6048</Text>
      </HStack>
    </Box>
  );
};

export default Contact;
